// Copyright 2016 Proyectos y Sistemas de Mantenimiento SL (eProsima).
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*!
 * @file Cv2xData.h
 * This header file contains the declaration of the described types in the IDL file.
 *
 * This file was generated by the tool fastddsgen.
 */

#ifndef _FAST_DDS_GENERATED_CV2XDATA_H_
#define _FAST_DDS_GENERATED_CV2XDATA_H_

#include <array>
#include <bitset>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

#include <fastcdr/cdr/fixed_size_string.hpp>
#include <fastcdr/xcdr/external.hpp>
#include <fastcdr/xcdr/optional.hpp>



#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#define eProsima_user_DllExport __declspec( dllexport )
#else
#define eProsima_user_DllExport
#endif  // EPROSIMA_USER_DLL_EXPORT
#else
#define eProsima_user_DllExport
#endif  // _WIN32

#if defined(_WIN32)
#if defined(EPROSIMA_USER_DLL_EXPORT)
#if defined(CV2XDATA_SOURCE)
#define CV2XDATA_DllAPI __declspec( dllexport )
#else
#define CV2XDATA_DllAPI __declspec( dllimport )
#endif // CV2XDATA_SOURCE
#else
#define CV2XDATA_DllAPI
#endif  // EPROSIMA_USER_DLL_EXPORT
#else
#define CV2XDATA_DllAPI
#endif // _WIN32

namespace eprosima {
namespace fastcdr {
class Cdr;
class CdrSizeCalculator;
} // namespace fastcdr
} // namespace eprosima







/*!
 * @brief This class represents the structure Cv2xData defined by the user in the IDL file.
 * @ingroup Cv2xData
 */
class Cv2xData
{
public:

    /*!
     * @brief Default constructor.
     */
    eProsima_user_DllExport Cv2xData();

    /*!
     * @brief Default destructor.
     */
    eProsima_user_DllExport ~Cv2xData();

    /*!
     * @brief Copy constructor.
     * @param x Reference to the object Cv2xData that will be copied.
     */
    eProsima_user_DllExport Cv2xData(
            const Cv2xData& x);

    /*!
     * @brief Move constructor.
     * @param x Reference to the object Cv2xData that will be copied.
     */
    eProsima_user_DllExport Cv2xData(
            Cv2xData&& x) noexcept;

    /*!
     * @brief Copy assignment.
     * @param x Reference to the object Cv2xData that will be copied.
     */
    eProsima_user_DllExport Cv2xData& operator =(
            const Cv2xData& x);

    /*!
     * @brief Move assignment.
     * @param x Reference to the object Cv2xData that will be copied.
     */
    eProsima_user_DllExport Cv2xData& operator =(
            Cv2xData&& x) noexcept;

    /*!
     * @brief Comparison operator.
     * @param x Cv2xData object to compare.
     */
    eProsima_user_DllExport bool operator ==(
            const Cv2xData& x) const;

    /*!
     * @brief Comparison operator.
     * @param x Cv2xData object to compare.
     */
    eProsima_user_DllExport bool operator !=(
            const Cv2xData& x) const;

    /*!
     * @brief This function sets a value in member len
     * @param _len New value for member len
     */
    eProsima_user_DllExport void len(
            uint32_t _len);

    /*!
     * @brief This function returns the value of member len
     * @return Value of member len
     */
    eProsima_user_DllExport uint32_t len() const;

    /*!
     * @brief This function returns a reference to member len
     * @return Reference to member len
     */
    eProsima_user_DllExport uint32_t& len();


    /*!
     * @brief This function copies the value in member src_id
     * @param _src_id New value to be copied in member src_id
     */
    eProsima_user_DllExport void src_id(
            const std::array<uint8_t, 3>& _src_id);

    /*!
     * @brief This function moves the value in member src_id
     * @param _src_id New value to be moved in member src_id
     */
    eProsima_user_DllExport void src_id(
            std::array<uint8_t, 3>&& _src_id);

    /*!
     * @brief This function returns a constant reference to member src_id
     * @return Constant reference to member src_id
     */
    eProsima_user_DllExport const std::array<uint8_t, 3>& src_id() const;

    /*!
     * @brief This function returns a reference to member src_id
     * @return Reference to member src_id
     */
    eProsima_user_DllExport std::array<uint8_t, 3>& src_id();


    /*!
     * @brief This function copies the value in member msg
     * @param _msg New value to be copied in member msg
     */
    eProsima_user_DllExport void msg(
            const std::vector<uint8_t>& _msg);

    /*!
     * @brief This function moves the value in member msg
     * @param _msg New value to be moved in member msg
     */
    eProsima_user_DllExport void msg(
            std::vector<uint8_t>&& _msg);

    /*!
     * @brief This function returns a constant reference to member msg
     * @return Constant reference to member msg
     */
    eProsima_user_DllExport const std::vector<uint8_t>& msg() const;

    /*!
     * @brief This function returns a reference to member msg
     * @return Reference to member msg
     */
    eProsima_user_DllExport std::vector<uint8_t>& msg();

private:

    uint32_t m_len{0};
    std::array<uint8_t, 3> m_src_id{0};
    std::vector<uint8_t> m_msg;

};

#endif // _FAST_DDS_GENERATED_CV2XDATA_H_



