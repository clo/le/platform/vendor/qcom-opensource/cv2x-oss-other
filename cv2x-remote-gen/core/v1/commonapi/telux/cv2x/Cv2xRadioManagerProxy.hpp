/*
* This file was generated by the CommonAPI Generators.
* Used org.genivi.commonapi.core 3.2.0.v202012010850.
* Used org.franca.core 0.13.1.201807231814.
*
* This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
* If a copy of the MPL was not distributed with this file, You can obtain one at
* http://mozilla.org/MPL/2.0/.
*/
#ifndef V1_COMMONAPI_TELUX_CV2X_Cv2x_Radio_Manager_PROXY_HPP_
#define V1_COMMONAPI_TELUX_CV2X_Cv2x_Radio_Manager_PROXY_HPP_

#include <v1/commonapi/telux/cv2x/Cv2xRadioManagerProxyBase.hpp>

#include "v1/commonapi/telux/cv2x/Cv2xRadioTypesProxy.hpp"

#if !defined (COMMONAPI_INTERNAL_COMPILATION)
#define COMMONAPI_INTERNAL_COMPILATION
#define HAS_DEFINED_COMMONAPI_INTERNAL_COMPILATION_HERE
#endif


#if defined (HAS_DEFINED_COMMONAPI_INTERNAL_COMPILATION_HERE)
#undef COMMONAPI_INTERNAL_COMPILATION
#undef HAS_DEFINED_COMMONAPI_INTERNAL_COMPILATION_HERE
#endif

namespace v1 {
namespace commonapi {
namespace telux {
namespace cv2x {

template <typename ... _AttributeExtensions>
class Cv2xRadioManagerProxy
    : virtual public Cv2xRadioManager,
      virtual public Cv2xRadioManagerProxyBase,
      public ::v1::commonapi::telux::cv2x::Cv2xRadioTypesProxy<_AttributeExtensions...>,
      virtual public _AttributeExtensions... {
public:
    Cv2xRadioManagerProxy(std::shared_ptr<CommonAPI::Proxy> delegate);
    ~Cv2xRadioManagerProxy();

    typedef Cv2xRadioManager InterfaceType;

    inline static const char* getInterface() {
        return(Cv2xRadioManager::getInterface());
    }

    /**
     * Returns the CommonAPI address of the remote partner this proxy communicates with.
     */
    virtual const CommonAPI::Address &getAddress() const;

    /**
     * Returns true if the remote partner for this proxy is currently known to be available.
     */
    virtual bool isAvailable() const;

    /**
     * Returns true if the remote partner for this proxy is available.
     */
    virtual bool isAvailableBlocking() const;

    /**
     * Returns the wrapper class that is used to (de-)register for notifications about
     * the availability of the remote partner of this proxy.
     */
    virtual CommonAPI::ProxyStatusEvent& getProxyStatusEvent();

    /**
     * Returns the wrapper class that is used to access version information of the remote
     * partner of this proxy.
     */
    virtual CommonAPI::InterfaceVersionAttribute& getInterfaceVersionAttribute();

    virtual std::future<void> getCompletionFuture();

    /**
     * Calls GetServiceStatus with synchronous semantics.
     *
     * All non-const parameters will be filled with the returned values.
     * The CallStatus will be filled when the method returns and indicate either
     * "SUCCESS" or which type of error has occurred. In case of an error, ONLY the CallStatus
     * will be set.
     */
    virtual void GetServiceStatus(CommonAPI::CallStatus &_internalCallStatus, ::v1::commonapi::telux::common::CommonDefines::ErrorCode &_error, ::v1::commonapi::telux::common::CommonDefines::ServiceStatus &_status, const CommonAPI::CallInfo *_info = nullptr);
    /**
     * Calls GetServiceStatus with asynchronous semantics.
     *
     * The provided callback will be called when the reply to this call arrives or
     * an error occurs during the call. The CallStatus will indicate either "SUCCESS"
     * or which type of error has occurred. In case of any error, ONLY the CallStatus
     * will have a defined value.
     * The std::future returned by this method will be fulfilled at arrival of the reply.
     * It will provide the same value for CallStatus as will be handed to the callback.
     */
    virtual std::future<CommonAPI::CallStatus> GetServiceStatusAsync(GetServiceStatusAsyncCallback _callback = nullptr, const CommonAPI::CallInfo *_info = nullptr);
    /**
     * Returns the wrapper class that provides access to the broadcast currentCv2xStatus.
     */
    virtual CurrentCv2xStatusEvent& getCurrentCv2xStatusEvent() {
        return delegate_->getCurrentCv2xStatusEvent();
    }
    /**
     * Calls startCv2x with synchronous semantics.
     *
     * All non-const parameters will be filled with the returned values.
     * The CallStatus will be filled when the method returns and indicate either
     * "SUCCESS" or which type of error has occurred. In case of an error, ONLY the CallStatus
     * will be set.
     */
    virtual void startCv2x(CommonAPI::CallStatus &_internalCallStatus, ::v1::commonapi::telux::common::CommonDefines::ErrorCode &_error, ::v1::commonapi::telux::common::CommonDefines::Status &_status, const CommonAPI::CallInfo *_info = nullptr);
    /**
     * Calls startCv2x with asynchronous semantics.
     *
     * The provided callback will be called when the reply to this call arrives or
     * an error occurs during the call. The CallStatus will indicate either "SUCCESS"
     * or which type of error has occurred. In case of any error, ONLY the CallStatus
     * will have a defined value.
     * The std::future returned by this method will be fulfilled at arrival of the reply.
     * It will provide the same value for CallStatus as will be handed to the callback.
     */
    virtual std::future<CommonAPI::CallStatus> startCv2xAsync(StartCv2xAsyncCallback _callback = nullptr, const CommonAPI::CallInfo *_info = nullptr);
    /**
     * Calls stopCv2x with synchronous semantics.
     *
     * All non-const parameters will be filled with the returned values.
     * The CallStatus will be filled when the method returns and indicate either
     * "SUCCESS" or which type of error has occurred. In case of an error, ONLY the CallStatus
     * will be set.
     */
    virtual void stopCv2x(CommonAPI::CallStatus &_internalCallStatus, ::v1::commonapi::telux::common::CommonDefines::ErrorCode &_error, ::v1::commonapi::telux::common::CommonDefines::Status &_status, const CommonAPI::CallInfo *_info = nullptr);
    /**
     * Calls stopCv2x with asynchronous semantics.
     *
     * The provided callback will be called when the reply to this call arrives or
     * an error occurs during the call. The CallStatus will indicate either "SUCCESS"
     * or which type of error has occurred. In case of any error, ONLY the CallStatus
     * will have a defined value.
     * The std::future returned by this method will be fulfilled at arrival of the reply.
     * It will provide the same value for CallStatus as will be handed to the callback.
     */
    virtual std::future<CommonAPI::CallStatus> stopCv2xAsync(StopCv2xAsyncCallback _callback = nullptr, const CommonAPI::CallInfo *_info = nullptr);
    /**
     * Calls requestCv2xStatus with synchronous semantics.
     *
     * All non-const parameters will be filled with the returned values.
     * The CallStatus will be filled when the method returns and indicate either
     * "SUCCESS" or which type of error has occurred. In case of an error, ONLY the CallStatus
     * will be set.
     */
    virtual void requestCv2xStatus(CommonAPI::CallStatus &_internalCallStatus, ::v1::commonapi::telux::common::CommonDefines::ErrorCode &_error, ::v1::commonapi::telux::cv2x::Cv2xRadioTypes::Cv2xStatus &_status, const CommonAPI::CallInfo *_info = nullptr);
    /**
     * Calls requestCv2xStatus with asynchronous semantics.
     *
     * The provided callback will be called when the reply to this call arrives or
     * an error occurs during the call. The CallStatus will indicate either "SUCCESS"
     * or which type of error has occurred. In case of any error, ONLY the CallStatus
     * will have a defined value.
     * The std::future returned by this method will be fulfilled at arrival of the reply.
     * It will provide the same value for CallStatus as will be handed to the callback.
     */
    virtual std::future<CommonAPI::CallStatus> requestCv2xStatusAsync(RequestCv2xStatusAsyncCallback _callback = nullptr, const CommonAPI::CallInfo *_info = nullptr);
    /**
     * Calls requestCv2xStatusEx with synchronous semantics.
     *
     * All non-const parameters will be filled with the returned values.
     * The CallStatus will be filled when the method returns and indicate either
     * "SUCCESS" or which type of error has occurred. In case of an error, ONLY the CallStatus
     * will be set.
     */
    virtual void requestCv2xStatusEx(CommonAPI::CallStatus &_internalCallStatus, ::v1::commonapi::telux::common::CommonDefines::ErrorCode &_error, ::v1::commonapi::telux::cv2x::Cv2xRadioTypes::Cv2xStatusEx &_statusEx, const CommonAPI::CallInfo *_info = nullptr);
    /**
     * Calls requestCv2xStatusEx with asynchronous semantics.
     *
     * The provided callback will be called when the reply to this call arrives or
     * an error occurs during the call. The CallStatus will indicate either "SUCCESS"
     * or which type of error has occurred. In case of any error, ONLY the CallStatus
     * will have a defined value.
     * The std::future returned by this method will be fulfilled at arrival of the reply.
     * It will provide the same value for CallStatus as will be handed to the callback.
     */
    virtual std::future<CommonAPI::CallStatus> requestCv2xStatusExAsync(RequestCv2xStatusExAsyncCallback _callback = nullptr, const CommonAPI::CallInfo *_info = nullptr);



 private:
    std::shared_ptr< Cv2xRadioManagerProxyBase> delegate_;
};

typedef Cv2xRadioManagerProxy<> Cv2xRadioManagerProxyDefault;


//
// Cv2xRadioManagerProxy Implementation
//
template <typename ... _AttributeExtensions>
Cv2xRadioManagerProxy<_AttributeExtensions...>::Cv2xRadioManagerProxy(std::shared_ptr<CommonAPI::Proxy> delegate):
        ::v1::commonapi::telux::cv2x::Cv2xRadioTypesProxy<_AttributeExtensions...>(delegate),
        _AttributeExtensions(*(std::dynamic_pointer_cast< Cv2xRadioManagerProxyBase>(delegate)))...,
        delegate_(std::dynamic_pointer_cast< Cv2xRadioManagerProxyBase>(delegate)) {
}

template <typename ... _AttributeExtensions>
Cv2xRadioManagerProxy<_AttributeExtensions...>::~Cv2xRadioManagerProxy() {
}

template <typename ... _AttributeExtensions>
void Cv2xRadioManagerProxy<_AttributeExtensions...>::GetServiceStatus(CommonAPI::CallStatus &_internalCallStatus, ::v1::commonapi::telux::common::CommonDefines::ErrorCode &_error, ::v1::commonapi::telux::common::CommonDefines::ServiceStatus &_status, const CommonAPI::CallInfo *_info) {
    delegate_->GetServiceStatus(_internalCallStatus, _error, _status, _info);
}

template <typename ... _AttributeExtensions>
std::future<CommonAPI::CallStatus> Cv2xRadioManagerProxy<_AttributeExtensions...>::GetServiceStatusAsync(GetServiceStatusAsyncCallback _callback, const CommonAPI::CallInfo *_info) {
    return delegate_->GetServiceStatusAsync(_callback, _info);
}
template <typename ... _AttributeExtensions>
void Cv2xRadioManagerProxy<_AttributeExtensions...>::startCv2x(CommonAPI::CallStatus &_internalCallStatus, ::v1::commonapi::telux::common::CommonDefines::ErrorCode &_error, ::v1::commonapi::telux::common::CommonDefines::Status &_status, const CommonAPI::CallInfo *_info) {
    delegate_->startCv2x(_internalCallStatus, _error, _status, _info);
}

template <typename ... _AttributeExtensions>
std::future<CommonAPI::CallStatus> Cv2xRadioManagerProxy<_AttributeExtensions...>::startCv2xAsync(StartCv2xAsyncCallback _callback, const CommonAPI::CallInfo *_info) {
    return delegate_->startCv2xAsync(_callback, _info);
}
template <typename ... _AttributeExtensions>
void Cv2xRadioManagerProxy<_AttributeExtensions...>::stopCv2x(CommonAPI::CallStatus &_internalCallStatus, ::v1::commonapi::telux::common::CommonDefines::ErrorCode &_error, ::v1::commonapi::telux::common::CommonDefines::Status &_status, const CommonAPI::CallInfo *_info) {
    delegate_->stopCv2x(_internalCallStatus, _error, _status, _info);
}

template <typename ... _AttributeExtensions>
std::future<CommonAPI::CallStatus> Cv2xRadioManagerProxy<_AttributeExtensions...>::stopCv2xAsync(StopCv2xAsyncCallback _callback, const CommonAPI::CallInfo *_info) {
    return delegate_->stopCv2xAsync(_callback, _info);
}
template <typename ... _AttributeExtensions>
void Cv2xRadioManagerProxy<_AttributeExtensions...>::requestCv2xStatus(CommonAPI::CallStatus &_internalCallStatus, ::v1::commonapi::telux::common::CommonDefines::ErrorCode &_error, ::v1::commonapi::telux::cv2x::Cv2xRadioTypes::Cv2xStatus &_status, const CommonAPI::CallInfo *_info) {
    delegate_->requestCv2xStatus(_internalCallStatus, _error, _status, _info);
}

template <typename ... _AttributeExtensions>
std::future<CommonAPI::CallStatus> Cv2xRadioManagerProxy<_AttributeExtensions...>::requestCv2xStatusAsync(RequestCv2xStatusAsyncCallback _callback, const CommonAPI::CallInfo *_info) {
    return delegate_->requestCv2xStatusAsync(_callback, _info);
}
template <typename ... _AttributeExtensions>
void Cv2xRadioManagerProxy<_AttributeExtensions...>::requestCv2xStatusEx(CommonAPI::CallStatus &_internalCallStatus, ::v1::commonapi::telux::common::CommonDefines::ErrorCode &_error, ::v1::commonapi::telux::cv2x::Cv2xRadioTypes::Cv2xStatusEx &_statusEx, const CommonAPI::CallInfo *_info) {
    delegate_->requestCv2xStatusEx(_internalCallStatus, _error, _statusEx, _info);
}

template <typename ... _AttributeExtensions>
std::future<CommonAPI::CallStatus> Cv2xRadioManagerProxy<_AttributeExtensions...>::requestCv2xStatusExAsync(RequestCv2xStatusExAsyncCallback _callback, const CommonAPI::CallInfo *_info) {
    return delegate_->requestCv2xStatusExAsync(_callback, _info);
}

template <typename ... _AttributeExtensions>
const CommonAPI::Address &Cv2xRadioManagerProxy<_AttributeExtensions...>::getAddress() const {
    return delegate_->getAddress();
}

template <typename ... _AttributeExtensions>
bool Cv2xRadioManagerProxy<_AttributeExtensions...>::isAvailable() const {
    return delegate_->isAvailable();
}

template <typename ... _AttributeExtensions>
bool Cv2xRadioManagerProxy<_AttributeExtensions...>::isAvailableBlocking() const {
    return delegate_->isAvailableBlocking();
}

template <typename ... _AttributeExtensions>
CommonAPI::ProxyStatusEvent& Cv2xRadioManagerProxy<_AttributeExtensions...>::getProxyStatusEvent() {
    return delegate_->getProxyStatusEvent();
}

template <typename ... _AttributeExtensions>
CommonAPI::InterfaceVersionAttribute& Cv2xRadioManagerProxy<_AttributeExtensions...>::getInterfaceVersionAttribute() {
    return delegate_->getInterfaceVersionAttribute();
}


template <typename ... _AttributeExtensions>
std::future<void> Cv2xRadioManagerProxy<_AttributeExtensions...>::getCompletionFuture() {
    return delegate_->getCompletionFuture();
}

} // namespace cv2x
} // namespace telux
} // namespace commonapi
} // namespace v1



// Compatibility
namespace v1_0 = v1;

#endif // V1_COMMONAPI_TELUX_CV2X_Cv2x_Radio_Manager_PROXY_HPP_
