/*
 * This file was generated by the CommonAPI Generators.
 * Used org.genivi.commonapi.someip 3.2.0.v202012010944.
 * Used org.franca.core 0.13.1.201807231814.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * http://mozilla.org/MPL/2.0/.
 */
#include <v1/commonapi/telux/common/CommonDefinesSomeIPProxy.hpp>

#if !defined (COMMONAPI_INTERNAL_COMPILATION)
#define COMMONAPI_INTERNAL_COMPILATION
#define HAS_DEFINED_COMMONAPI_INTERNAL_COMPILATION_HERE
#endif

#include <CommonAPI/SomeIP/AddressTranslator.hpp>

#if defined (HAS_DEFINED_COMMONAPI_INTERNAL_COMPILATION_HERE)
#undef COMMONAPI_INTERNAL_COMPILATION
#undef HAS_DEFINED_COMMONAPI_INTERNAL_COMPILATION_HERE
#endif

namespace v1 {
namespace commonapi {
namespace telux {
namespace common {

std::shared_ptr<CommonAPI::SomeIP::Proxy> createCommonDefinesSomeIPProxy(
    const CommonAPI::SomeIP::Address &_address,
    const std::shared_ptr<CommonAPI::SomeIP::ProxyConnection> &_connection) {
    return std::make_shared< CommonDefinesSomeIPProxy>(_address, _connection);
}

void initializeCommonDefinesSomeIPProxy() {
    CommonAPI::SomeIP::Factory::get()->registerProxyCreateMethod(
        "commonapi.telux.common.CommonDefines:v1_0",
        &createCommonDefinesSomeIPProxy);
}

INITIALIZER(registerCommonDefinesSomeIPProxy) {
    CommonAPI::SomeIP::Factory::get()->registerInterface(initializeCommonDefinesSomeIPProxy);
}

CommonDefinesSomeIPProxy::CommonDefinesSomeIPProxy(
    const CommonAPI::SomeIP::Address &_address,
    const std::shared_ptr<CommonAPI::SomeIP::ProxyConnection> &_connection)
        : CommonAPI::SomeIP::Proxy(_address, _connection)
{
}

CommonDefinesSomeIPProxy::~CommonDefinesSomeIPProxy() {
    completed_.set_value();
}



void CommonDefinesSomeIPProxy::getOwnVersion(uint16_t& ownVersionMajor, uint16_t& ownVersionMinor) const {
    ownVersionMajor = 1;
    ownVersionMinor = 0;
}

std::future<void> CommonDefinesSomeIPProxy::getCompletionFuture() {
    return completed_.get_future();
}

} // namespace common
} // namespace telux
} // namespace commonapi
} // namespace v1
